package com.interview.goeuro.service;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.interview.goeuro.repository.RoutesRepository;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class RoutesManagerServiceTest {

    private static final Logger logger = Logger.getLogger(RoutesManagerServiceTest.class);

    @Mock
    private RoutesRepository repo;
    private RouteManagerService rms;

    @Before
    public void setUp() {
        rms = new RouteManagerService(repo);
    }

    @Test
    public void thereExistARouteTest() {
        logger.info("Testing and comparing actual route and computed route between 2 stations");
        when(repo.findRoutesBetweenStations(anyInt(), anyInt())).thenReturn(Arrays.asList(1));
        boolean isRouteAvailable = rms.isDirectRouteBetweenStations(3, 6);
        Assertions.assertThat(isRouteAvailable).isTrue();
    }

    @Test
    public void thereExistNoRouteTest() {
        logger.info("Testing and comparing when no route exist between 2 stations");
        when(repo.findRoutesBetweenStations(anyInt(), anyInt())).thenReturn(new ArrayList<Integer>());
        boolean isRouteAvailable = rms.isDirectRouteBetweenStations(3, 2);
        Assertions.assertThat(isRouteAvailable).isFalse();
    }
}
