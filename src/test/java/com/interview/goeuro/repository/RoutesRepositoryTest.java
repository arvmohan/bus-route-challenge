package com.interview.goeuro.repository;

import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.interview.goeuro.helper.FileParser;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class RoutesRepositoryTest {

    private static final Logger logger = Logger.getLogger(RoutesRepositoryTest.class);
    @Mock
    private FileParser fp;

    @Mock
    private RoutesRepository repo;

    @Test
    public void routeInformationBetweenTwoStationsTest() {
        logger.info("Testing correctness of route information between 2 stations");
        when(repo.findRoutesBetweenStations(1, 2)).thenReturn(Arrays.asList(0, 1));
        Assertions.assertThat(repo.findRoutesBetweenStations(1, 2).size() > 0).isTrue();
    }
}
