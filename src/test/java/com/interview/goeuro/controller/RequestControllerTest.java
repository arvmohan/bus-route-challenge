package com.interview.goeuro.controller;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.interview.goeuro.service.RouteManagerService;

/**
 * Integration test to verify endpoints
 * 
 * @author Arvind
 *
 */

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class RequestControllerTest {

    private static final Logger logger = Logger.getLogger(RequestControllerTest.class);

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("dataPath", "/src/test/resources/data/example");
    }

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RouteManagerService rms;

    @Test
    public void incompleteRequestNoParameter() throws Exception {
        logger.info("Testing when No request parameter is provided");
        this.mockMvc.perform(get("/api/direct")).andExpect(status().isBadRequest()).andExpect(status().reason("Required int parameter 'dep_sid' is not present"));
    }

    @Test
    public void incompleteRequestNoDepartureStationId() throws Exception {
        logger.info("Testing when only arrival station id is provided");
        this.mockMvc.perform(get("/api/direct").param("arr_sid", "0")).andExpect(status().isBadRequest()).andExpect(status().reason("Required int parameter 'dep_sid' is not present"));
    }

    @Test
    public void incompleteRequestNoArrivalStationId() throws Exception {
        logger.info("Testing when only departure station id is provided");
        this.mockMvc.perform(get("/api/direct").param("dep_sid", "0")).andExpect(status().isBadRequest()).andExpect(status().reason("Required int parameter 'arr_sid' is not present"));
    }

    @Test
    public void thereExistARouteResponseTest() throws Exception {
        when(rms.getRouteBetweenStations(anyInt(), anyInt())).thenReturn(Arrays.asList(1));
        this.mockMvc.perform(get("/api/direct").param("dep_sid", "3").param("arr_sid", "6")).andExpect(content().contentType("application/json;charset=UTF-8")).andExpect(status().isOk()).andExpect(jsonPath("$.dep_sid").value("3")).andExpect(jsonPath("$.arr_sid").value("6")).andExpect(jsonPath("$.direct_bus_route").value("true"));
    }

    @Test
    public void noRouteExistResponseTest() throws Exception {
        when(rms.isDirectRouteBetweenStations(anyInt(), anyInt())).thenReturn(false);
        this.mockMvc.perform(get("/api/direct").param("dep_sid", "10").param("arr_sid", "6")).andExpect(content().contentType("application/json;charset=UTF-8")).andExpect(status().isOk()).andExpect(jsonPath("$.dep_sid").value("10")).andExpect(jsonPath("$.arr_sid").value("6")).andExpect(jsonPath("$.direct_bus_route").value("false"));
    }
}
