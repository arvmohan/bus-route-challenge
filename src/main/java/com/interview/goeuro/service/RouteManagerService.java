package com.interview.goeuro.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.interview.goeuro.controller.RequestController;
import com.interview.goeuro.repository.RoutesRepository;

/**
 * 
 * 
 * {@link RouteManagerService} is responsible for aggregating the response
 * corresponding to request made by the {@link RequestController}
 */
@Service
public class RouteManagerService {

    RoutesRepository repo;

    @Autowired
    public RouteManagerService(RoutesRepository rs) {
        this.repo = rs;
    }

    public boolean isDirectRouteBetweenStations(int depStationId, int arrStationId) {
        List<Integer> possibleRoutes = repo.findRoutesBetweenStations(depStationId, arrStationId);
        return possibleRoutes.size() > 0;
    }

    /**
     * Extension from the problem statement, to find possible routes between 2
     * stations
     * 
     * @param depStationId
     * @param arrStationId
     * @return
     */
    public List<Integer> getRouteBetweenStations(int depStationId, int arrStationId) {
        List<Integer> possibleRoutes = repo.findRoutesBetweenStations(depStationId, arrStationId);
        return possibleRoutes;
    }
}
