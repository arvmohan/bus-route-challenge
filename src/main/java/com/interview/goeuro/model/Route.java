package com.interview.goeuro.model;

import java.util.HashSet;
import java.util.List;

import lombok.Value;

/**
 * NO LONGER NEEDED.
 */

/**
 * 
 * {@link Route} is a POJO consisting of route id and a list of station ids
 */
@Value
public class Route extends Object {
    int id;
    List<Integer> stations;

    public Route(int id, List<Integer> stations) {
        checkIfDuplicateStation(stations);
        this.id = id;
        this.stations = stations;
    }

    public int getId() {
        return id;
    }

    public List<Integer> getStations() {
        return stations;
    }

    private void checkIfDuplicateStation(List<Integer> stations) {
        if (stations.size() > new HashSet<>(stations).size()) {
            throw new RuntimeException("Route " + id + " contains one or more duplicate stations");
        }
    }
}
