package com.interview.goeuro.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * {@link Response} qualifies the required response object as required by bus
 * route challenge
 */
public class Response {

    int depStationId;

    int arrStationId;

    boolean isDirectBusRoute;

    List<Integer> routes;

    public Response(int depId, int arrId, boolean isDirectBusRoute, List<Integer> routes) {
        this.depStationId = depId;
        this.arrStationId = arrId;
        this.isDirectBusRoute = isDirectBusRoute;
        this.routes = routes;
    }

    @JsonProperty("dep_sid")
    public int getDepStationId() {
        return depStationId;
    }

    @JsonProperty("arr_sid")
    public int getArrStationId() {
        return arrStationId;
    }

    @JsonProperty("direct_bus_route")
    public boolean isDirectBusRoute() {
        return isDirectBusRoute;
    }

    @JsonProperty("routes")
    public List<Integer> getRoutes() {
        return routes;
    }
}
