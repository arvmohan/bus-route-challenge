package com.interview.goeuro.controller;

import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.interview.goeuro.model.Response;
import com.interview.goeuro.service.RouteManagerService;

/**
 * 
 * RequestController class is responsible for handling HTTP request in our
 * bus-route-challenge web-service
 */
@RestController
public class RequestController {

    private static final Logger logger = Logger.getLogger(RequestController.class);
    RouteManagerService rms;

    @Autowired
    public RequestController(RouteManagerService rms) {
        this.rms = rms;
    }

    /**
     * 
     * @param depStationId
     *            Departure station id
     * @param arrStationId
     *            Arrival station id
     * @return {@link Response} object which satisfies the challenge
     *         requirements
     */
    @RequestMapping(method = RequestMethod.GET, path = "/api/direct")
    public Response getDirectRoute(@RequestParam("dep_sid") int depStationId, @RequestParam("arr_sid") int arrStationId) {
        logger.info("Requested a bus route between station id = " + depStationId + " & station id = " + arrStationId);
        List<Integer> possibleRoutes = rms.getRouteBetweenStations(depStationId, arrStationId);
        return new Response(depStationId, arrStationId, possibleRoutes.size() > 0, possibleRoutes);
    }
}
