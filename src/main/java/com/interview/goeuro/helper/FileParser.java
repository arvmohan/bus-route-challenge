package com.interview.goeuro.helper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.interview.goeuro.constants.Constants;
import com.interview.goeuro.constants.RoutesInputErrorType;
import com.interview.goeuro.exceptions.InvalidInputException;
import com.interview.goeuro.model.Route;

/**
 * 
 * {@link FileParser} reads the routes data from a data file and massages it
 * into a list of {@link Route}
 */
@Component
public class FileParser {

    @Value("${dataPath:data/example}")
    String filePath;

    private static final Pattern WHITE_SPACE_REGEX = Pattern.compile("\\s+");

    public Map<Integer, Map<Integer, List<Integer>>> routes = new HashMap<>();
    /**
     * 
     * @return List of {@link Route} as provided in the data file
     * @throws IOException
     */
    public void validateAndInitialize() throws IOException {
        List<String> routesInfo = Files.readAllLines(Paths.get(filePath));

        // Check for total number of routes defined in the first line and total
        // number of routes found
        checkNumberOfRoutes(Integer.valueOf(routesInfo.get(0)), routesInfo.size() - 1);

        // Check if same route id is repeated
        checkDuplicateRouteIds(routesInfo);

        // Check for duplicate stations in a route
        checkRoutesWithDuplicateStations(routesInfo);

        // Check if total number of routes is greater than the sanctioned
        // maximum number of routes
        checkRoutesMaxCount(Integer.valueOf(routesInfo.get(0)));

        // Check if total number of stations in any route is greater than the
        // sanctioned maximum number of stations
        checkStationMaxCount(routesInfo);

        // compute the map of station id vs (a map of connected stations vs the
        // route via which they are connected)
        this.routes = map(routesInfo.subList(1, routesInfo.size()));
    }

    // Iterate over routes list and for each route, update its stations'
    // connected stations
    public Map<Integer, Map<Integer, List<Integer>>> map(final List<String> routesInfo) {
        Map<Integer, Map<Integer, List<Integer>>> result = new HashMap<Integer, Map<Integer, List<Integer>>>();
        routesInfo.stream().filter(route -> !StringUtils.isEmpty(route)).map(route -> WHITE_SPACE_REGEX.splitAsStream(route).map(Integer::valueOf).collect(Collectors.toList())).forEach(route -> updateRouteMap(route, result));;
        return result;
    }

    // Iterate over a route and update its stations' connected stations and
    // corresponding route in global map
    private void updateRouteMap(final List<Integer> route, final Map<Integer, Map<Integer, List<Integer>>> result) {
        int routeId = route.get(0);
        List<Integer> stations = route.subList(1, route.size());
        IntStream.range(0, stations.size()).forEach(index -> updateGlobalMap(routeId, stations.get(index), stations.subList(index + 1, stations.size()), result));
    }

    // Update a station's connected stations and the corresponding route in the
    // global map
    private void updateGlobalMap(final int routeId, final int sourceStationId, List<Integer> connectedStations, final Map<Integer, Map<Integer, List<Integer>>> result) {
        if (!result.containsKey(sourceStationId)) {
            result.put(sourceStationId, new HashMap<>());
        }
        final Map<Integer, List<Integer>> oldConnectedStations = result.get(sourceStationId);
        connectedStations.stream().forEach(connectedStationId -> appendNewStations(connectedStationId, routeId, oldConnectedStations));
    }

    // Append a station in connected stations map
    private void appendNewStations(final int connectedStationId, final int routeId, final Map<Integer, List<Integer>> oldStations) {
        if (!oldStations.containsKey(connectedStationId)) {
            oldStations.put(connectedStationId, new ArrayList<Integer>());
        }
        oldStations.get(connectedStationId).add(routeId);
    }

    private void checkNumberOfRoutes(final int supposedlyNumberOfRoutes, final int actualNumberOfRoutes) {
        if (supposedlyNumberOfRoutes != actualNumberOfRoutes) {
            throw new InvalidInputException(RoutesInputErrorType.ROUTES_COUNT_MISMATCH);
        }
    }

    private void checkRoutesWithDuplicateStations(final List<String> routesInfo) {
        boolean duplicateStationPresent = routesInfo.stream().filter(route -> !StringUtils.isEmpty(route)).skip(1).map(route -> WHITE_SPACE_REGEX.splitAsStream(route).skip(1).map(Integer::valueOf).collect(Collectors.toList())).anyMatch(stationIds -> (stationIds.size() != (new HashSet<Integer>(stationIds)).size()));

        if (duplicateStationPresent) {
            throw new InvalidInputException(RoutesInputErrorType.DUPLICATE_STATIONS_FOUND_IN_A_ROUTE);
        }
    }

    private void checkDuplicateRouteIds(final List<String> routesInfo) {
        Set<Integer> uniqueRoutes = routesInfo.stream().filter(route -> !StringUtils.isEmpty(route)).skip(1).map(route -> WHITE_SPACE_REGEX.split(route)[0]).map(Integer::valueOf).collect(Collectors.toSet());
        if (uniqueRoutes.size() != routesInfo.size() - 1) {
            throw new InvalidInputException(RoutesInputErrorType.DUPLICATE_ROUTE_ID_FOUND);
        }
    }

    private void checkRoutesMaxCount(final long totalRoutesGiven) {
        if (totalRoutesGiven > Constants.MAX_ROUTES) {
            throw new InvalidInputException(RoutesInputErrorType.NUMBER_OF_ROUTES_EXCEEDED);
        }
    }

    private void checkStationMaxCount(final List<String> routesInfo) {
        final Set<Integer> uniqueStations = routesInfo.stream().filter(route -> !StringUtils.isEmpty(route)).skip(1).map(route -> WHITE_SPACE_REGEX.splitAsStream(route).skip(1).map(Integer::valueOf).collect(Collectors.toList())).flatMap(x -> x.stream()).collect(Collectors.toSet());

        if (uniqueStations.size() > Constants.MAX_UNIQUE_STATIONS) {
            throw new InvalidInputException(RoutesInputErrorType.NUMBER_OF_UNIQUE_STATIONS_EXCEEDED);
        }
    }
}