package com.interview.goeuro.exceptions;

import org.apache.log4j.Logger;

import com.interview.goeuro.constants.RoutesInputErrorType;

public class InvalidInputException extends IllegalArgumentException {

    private static final long serialVersionUID = -8882427910939217498L;
    private static final Logger logger = Logger.getLogger(InvalidInputException.class);

    RoutesInputErrorType error;
    public InvalidInputException(final RoutesInputErrorType errorType) {
        super(errorType.getMessage());
        logger.debug(errorType.getMessage());
        this.error = errorType;
    }

    public RoutesInputErrorType getErrorType() {
        return error;
    }
}
