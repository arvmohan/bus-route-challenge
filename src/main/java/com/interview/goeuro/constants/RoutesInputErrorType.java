package com.interview.goeuro.constants;

public enum RoutesInputErrorType {

    ROUTES_COUNT_MISMATCH(100, Constants.ROUTES_COUNT_MISMATCH_REASON,
            Constants.ROUTES_COUNT_MISMATCH_SUGGESTION),

    DUPLICATE_ROUTE_ID_FOUND(110, Constants.DUPLICATE_ROUTE_ID_FOUND_REASON,
            Constants.DUPLICATE_ROUTE_ID_FOUND_SUGGESTION),

    DUPLICATE_STATIONS_FOUND_IN_A_ROUTE(120,
            Constants.DUPLICATE_STATIONS_FOUND_IN_A_ROUTE_REASON,
            Constants.DUPLICATE_STATIONS_FOUND_IN_A_ROUTE_SUGGESTION),

    NUMBER_OF_ROUTES_EXCEEDED(130, Constants.NUMBER_OF_ROUTES_EXCEEDED_REASON,
            Constants.NUMBER_OF_ROUTES_EXCEEDED_SUGGESTION),

    NUMBER_OF_UNIQUE_STATIONS_EXCEEDED(140,
            Constants.NUMBER_OF_UNIQUE_STATIONS_EXCEEDED_REASON,
            Constants.NUMBER_OF_UNIQUE_STATIONS_EXCEEDED_SUGGESTION);

    private RoutesInputErrorType(int errorCode, String reason, String suggestion) {
        this.errorCode = errorCode;
        this.reason = reason;
        this.suggestion = suggestion;
    }

    private final int errorCode;
    private final String reason;
    private final String suggestion;

    public int getErrorCode() {
        return errorCode;
    }

    public String getReason() {
        return reason;
    }

    public String getSuggestion() {
        return suggestion;
    }

    /**
     * @return Formatted message corresponding to an error defined above
     */
    public String getMessage() {
        return String.format("Error Code: %s, Reason: %s, Suggestion: %s.", this.errorCode, this.reason, this.suggestion);
    }

}