package com.interview.goeuro.constants;

public final class Constants {
    // Upper limit on number of routes
    public final static int MAX_ROUTES = 100000;

    // Upper limit on number of stations allowed on a route
    public final static int MAX_STATIONS_ON_A_ROUTE = 1000;

    // Upper limited on unique stations allowed in the data set
    public final static int MAX_UNIQUE_STATIONS = 1000000;

    public static String ROUTES_COUNT_MISMATCH_REASON = "Number of routes provided in the data file is different from pre-defined number of routes on the first line of data file.";
    public static String ROUTES_COUNT_MISMATCH_SUGGESTION = "Make sure to provide same amout of routes as defined on the first line of data file.";

    public static String DUPLICATE_ROUTE_ID_FOUND_REASON = "Duplicate route ID found.";
    public static String DUPLICATE_ROUTE_ID_FOUND_SUGGESTION = "Make sure to have unique route Ids.";

    public static String DUPLICATE_STATIONS_FOUND_IN_A_ROUTE_REASON = "A route has duplicate stations.";
    public static String DUPLICATE_STATIONS_FOUND_IN_A_ROUTE_SUGGESTION = "Make sure to have only unique stations in every route.";

    public static String NUMBER_OF_ROUTES_EXCEEDED_REASON = "Total number of routes in data file exceeded the allowed limit.";
    public static String NUMBER_OF_ROUTES_EXCEEDED_SUGGESTION = String.format("Make sure that total number of routes doesn't exceed %s.", Constants.MAX_ROUTES);

    public static String NUMBER_OF_UNIQUE_STATIONS_EXCEEDED_REASON = "Total number of unique stations exceeded the allowed limit.";
    public static String NUMBER_OF_UNIQUE_STATIONS_EXCEEDED_SUGGESTION = String.format("Make sure that total number of unique stations doesn't exceed %s.", Constants.MAX_UNIQUE_STATIONS);

}
