package com.interview.goeuro.repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.interview.goeuro.helper.FileParser;
import com.interview.goeuro.model.Route;

/**
 * 
 * {@link RoutesRepository} class is responsible for initialising
 * {@link FileParser}, setting a list of {@link Route} and finding possible
 * routes between a pair of stations {@link RoutesRepository} would be
 * responsible for any communication with data store layer
 */
@Repository
public class RoutesRepository {

    FileParser fileParser;
    Map<Integer, Map<Integer, List<Integer>>> routes;

    @Autowired
    public RoutesRepository(FileParser fp) {
        try {
            this.fileParser = fp;
            fileParser.validateAndInitialize();
        } catch (IOException e) {

        }
    }

    /**
     * 
     * @param depStationId
     * @param arrStationId
     * @return
     */
    public List<Integer> findRoutesBetweenStations(int depStationId, int arrStationId) {
        return fileParser.routes.containsKey(depStationId) && fileParser.routes.get(depStationId).containsKey(arrStationId) ? fileParser.routes.get(depStationId).get(arrStationId) : new ArrayList<>();
    }
}
