# Bus Route Challenge

## Solution Approach
### First iteration
I maintained a list of routes, where route is a POJO consisting of an id and a list of stations. Whenever I need to figure out whether there is a route between 2 stations or not, I would iterate over this list of routes. It would equivalent to searching all the stations in every route, everytime I get a query to find existence of a route between 2 stations.
This would consume a lot of time, if data size is big enough.

### Second iteration
Instead of maintaining a list of routes, I started maintaining a `HashMap<Integer, HashMap<Integer, List<Integer>>>`
Key in outer hashmap would be Departure Station id
Key in inner hashmap would be Arrival Station id
Value in the inner hashmap would be list of routes connecting departure stationa and arrival station

In this way, after maintaining this in memory data store, I need not worry about any time conmusing request, because there would be none.
This solution is extensible, as well as performant.

## Running the solution

### Buiding the code

1. Clone the repo at https://gitlab.com/arvmohan/bus-route-challenge
2. Run `./service.sh dev_build`
3. Run `./service.sh dev_run`


### Using Docker Image
1. Clone docker image from `arvmohan/goeuro`
2. Run the image at port definition 8088:8088

### Configuration
With the assumption that you'd perform all the operation at repository root level
1. Default location of routes data is data/example
2. To change the location of routes data stop the solution and restart it with `./service.sh dev_run [PATH_TO_ROUTES_DATA_FILE]`
3. In case of running the jar files on your own, execute this ``java -DdataPath=[PATH_TO_ROUTES_DATA_FILE] -jar target/bus-route-challenge-*.jar`
where `dataPath` is the runtime variable, that we would be passing if we want to provide a custom file path.

## Control flow
REST Endpoint -> Request Controller -> RouteManagementService -> Repository Service -> FileParser

## Tech Stack
- Springboot
- JUnit
- Mockito
This solution leverages Spring-boot framework to provide a solution for bus-route-challenge problem. If the 2 input station id are present on one single route, only then it would return that there exists a route between those 2 stations.

## Future Improvements
Currently, if 2 stations are on the same route, they are shown as connected. For the case where one station is reachable from other station via going through multiple routes, we would need to maitain a graph (or a adjacency list).
Applying any search algorithm would solve the purpose, i.e. BFS or DFS would help figure out whether 2 stations are connected. Or while maintaining the map structure, we can build a complex logic to figure that out.

To know more about this problem please follow this link - https://github.com/goeuro/challenges/tree/master/bus_route_challenge